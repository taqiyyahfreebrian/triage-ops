# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../lib/generate/stage_policy'

RSpec.describe Generate::StagePolicy do
  let(:options) { double(:options, template: template_path, only: ['plan'], assign: nil) }
  let(:template_path) { 'path/to/template' }
  let(:template_name) { File.basename(template_path) }
  let(:output_dir) { "#{described_class.destination}/#{template_name}" }
  let(:stage) { 'plan' }
  let(:stage_definition) { {} }

  before do
    expect(File)
      .to receive(:read)
      .with(template_path)
      .and_return(<<~ERB)
        <%= stage_method_name %>
        <%= stage_label_name %>
        <%= assignees %>
        <%= groups %>
      ERB

    expect(FileUtils)
      .to receive(:rm_r)

    expect(FileUtils)
      .to receive(:mkdir_p)
      .with(output_dir)

    stub_const('StageDefinition::STAGE_DATA', stage => stage_definition)
  end

  describe '.run' do
    context 'when stage is allowlisted' do
      let(:stage) { 'plan' }

      it 'generates and writes policy files' do
        expect(StageDefinition)
          .to receive(:public_send)
          .with("stage_#{stage}", nil)
          .and_return({ labels: ["devops::#{stage}"], groups: 'fake_groups', assignees: 'fake_assignees' })

        expect(File)
          .to receive(:write)
          .with(
            "#{output_dir}/#{stage}.yml",
            <<~MARKDOWN)
              stage_#{stage}
              devops::#{stage}
              fake_assignees
              fake_groups
            MARKDOWN

        described_class.run(options)
      end
    end

    context 'when stage is not allowed in allowlist' do
      let(:stage) { 'invalid' }

      it 'does not generate nor write policy files' do
        expect(StageDefinition)
          .not_to receive(:public_send)

        expect(File)
          .not_to receive(:write)

        described_class.run(options)
      end
    end

    context 'when assignees are constrained with a commandline option' do
      let(:options) { double(:options, template: template_path, only: ['plan'], assign: ['backend_engineering_manager']) }

      it 'retrieves stage definitions with limited assignees' do
        expect(StageDefinition)
          .to receive(:public_send)
          .with("stage_#{stage}", ['backend_engineering_manager'])
          .and_return({ labels: ["devops::#{stage}"], groups: 'fake_groups', assignees: 'fake_assignees' })

        expect(File)
          .to receive(:write)

        described_class.run(options)
      end
    end
  end
end
