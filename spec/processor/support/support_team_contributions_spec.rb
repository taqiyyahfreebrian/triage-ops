# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/support/support_team_contributions'

RSpec.describe Triage::SupportTeamContributions do
  let(:from_gitlab_org) { false }
  let(:from_support_team_member) { false }
  let(:label_names) { [] }

  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'open',
        from_gitlab_org?: from_gitlab_org,
        label_names: label_names
      }
    end
  end

  subject { described_class.new(event) }

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }

      include_examples 'event is not applicable'
    end

    context 'when the event is not from a support team member' do
      let(:from_support_team_member) { false }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    let(:from_gitlab_org) { true }

    context 'when the event is from a support team member' do
      let(:from_support_team_member) { true }

      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          /label ~"#{Triage::SupportTeamContributions::SUPPORT_TEAM_CONTRIBUTIONS_LABEL}"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when the event already has the support label' do
      let(:label_names) { [Triage::SupportTeamContributions::SUPPORT_TEAM_CONTRIBUTIONS_LABEL] }

      it 'no comment is posted' do
        expect_no_request { subject.process }
      end
    end
  end
end
