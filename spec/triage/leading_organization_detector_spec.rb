# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/leading_organization_detector'

RSpec.describe Triage::LeadingOrganizationDetector, :clean_cache do
  let(:user_id) { 1 }
  let(:user_id_no_resident) { 3 }
  let(:csv_map) do
    [
      { 'AUTHOR_ID' => '1', 'AUTHOR_USERNAME' => 'johndoe' },
      { 'AUTHOR_ID' => '2', 'AUTHOR_USERNAME' => 'janedoe' }
    ]
  end

  describe '#leading_organization?' do
    before do
      allow(subject).to receive(:csv_map).and_return(csv_map)
    end

    context 'with valid user id' do
      context 'when present in the CSV' do
        it 'returns true' do
          expect(subject.leading_organization?(user_id)).to be(true)
        end
      end

      context 'when not present in the CSV' do
        let(:user_id) { user_id_no_resident }

        it 'returns false' do
          expect(subject.leading_organization?(user_id)).to be(false)
        end
      end
    end

    context 'when user_id is nil' do
      let(:user_id) { nil }

      it 'returns false' do
        expect(subject.leading_organization?(user_id)).to be(false)
      end
    end
  end
end
