# frozen_string_literal: true

require 'spec_helper'
require_relative '../../../triage/triage'
require_relative '../../../triage/resources/merge_request'

RSpec.describe Triage::MergeRequest do
  let(:project_id) { 222 }
  let(:iid) { 123 }
  let(:labels) { %w[foo bar] }
  let(:mr_attrs) do
    {
      iid: iid,
      project_id: project_id,
      labels: labels
    }
  end

  subject { described_class.new(project_id, iid) }

  describe '#label_names' do
    it 'makes a request to the relevant endpoint and returns the relevant labels' do
      expect_api_request(path: "/projects/#{project_id}/merge_requests/#{iid}", response_body: { 'labels' => labels }) do
        expect(subject.label_names).to eq(labels)
      end
    end
  end
end
