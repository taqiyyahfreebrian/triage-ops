# frozen_string_literal: true

require_relative '../../triage/event'
require_relative '../../triage/processor'

module Triage
  class SupportTeamContributions < Processor
    SUPPORT_TEAM_CONTRIBUTIONS_LABEL = 'Support Team Contributions'

    react_to 'merge_request.open'

    def applicable?
      event.from_gitlab_org? &&
        from_support_team_member?
    end

    def process
      apply_support_team_contributions_label
    end

    private

    def apply_support_team_contributions_label
      add_comment(%(/label ~"#{SUPPORT_TEAM_CONTRIBUTIONS_LABEL}"), append_source_link: false) unless has_label?(SUPPORT_TEAM_CONTRIBUTIONS_LABEL)
    end

    def has_label?(label)
      event.label_names.include?(label)
    end

    def from_support_team_member?(user_id = event.event_actor_id)
      Triage.gitlab_com_support_member_ids.include?(user_id)
    end
  end
end
